DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
                            `role_id` int NOT NULL AUTO_INCREMENT,
                            `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                            `role_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                            `role_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                            `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                            `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                            `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                            PRIMARY KEY (`role_id`),
                            UNIQUE KEY `role_idx1_role_code` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='系统角色表';
BEGIN;
INSERT INTO `sys_role` VALUES (1, '管理员', 'ADMIN', '管理员', '0', '2017-10-29 15:45:51', '2018-12-26 14:09:11', NULL, NULL);
COMMIT;

INSERT INTO `sys_role` VALUES (2, '用户', 'USER', '用户', '0', '2017-10-29 15:45:51', '2018-12-26 14:09:11', NULL, NULL);
COMMIT;

drop table if exists `sys_user_role`;
create table `sys_user_role` (
               user_id   varchar(50) not null comment '用户ID',
               role_id   bigint(20) not null comment '角色ID',
               primary key(user_id, role_id)
) engine=innodb comment = '用户和角色关联表';

insert into `sys_user_role` values ('2e041e67-e264-4d28-a1ab-c3e187ac140c', '1');
insert into `sys_user_role` values ('2e041e67-e264-4d28-a1ab-c3e187ac140c', '2');


insert into `sys_user_role` values ('8cf83984-9a7b-4a54-a7b4-63acf2791397', '1');
insert into `sys_user_role` values ('8cf83984-9a7b-4a54-a7b4-63acf2791397', '2');

drop table if exists `sys_uri`;
create table `sys_uri`(
    `id` int primary key  auto_increment comment '自动生成的主键ID',
    `uri` varchar(100) not null  comment '设置系统的持有的uri' ,
    `remark` varchar(100) comment '备注'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统url表';

insert into sys_uri(uri, remark)  value ('/api-website/hello' , '测试hello uri');
insert into sys_uri(uri, remark)  value ('/api-website/echo/**' , '测试hello uri');
insert into sys_uri(uri, remark)  value ('/api-work/echo/**' , '测试hello uri');
insert into sys_uri(uri, remark)  value ('/api-website/user/currentUser' , '测试hello uri');


drop table if exists `sys_role_uri`;
create table `sys_role_uri` (
       `role_id`   bigint(20) not null comment '角色ID',
       `sys_uri_id`   bigint(20) not null comment 'uriID',
       primary key(`role_id`, `sys_uri_id`)
) engine=innodb comment = '角色和请求路径关联表';

insert into sys_role_uri (role_id, sys_uri_id) value (1 , 1);
insert into sys_role_uri (role_id, sys_uri_id) value (1 , 2);
insert into sys_role_uri (role_id, sys_uri_id) value (1 , 3);
insert into sys_role_uri (role_id, sys_uri_id) value (1 , 4);
insert into sys_role_uri (role_id, sys_uri_id) value (2 , 4);
