import com.test.Oauth2ServerMain;
import com.test.entity.SysRole;
import com.test.entity.SysUri;
import com.test.mapper.UserMapper;
import com.test.service.SysRoleService;
import com.test.service.SysUriService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = Oauth2ServerMain.class)
public class AuthorityTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysUriService uriService;

    @Test
    public void test1() {
        List<String> userRoles = userMapper.getUserRoles("2e041e67-e264-4d28-a1ab-c3e187ac140c");
        userRoles.forEach(System.out::println);
    }

    @Test
    public void test2() {
        SysRole byId = roleService.getById(1);
        System.out.println(byId);
    }

    @Test
    public void test3() {
        SysUri byId = uriService.getById(1);
        System.out.println(byId);
    }

    @Test
    public void test4() {
        uriService.getAllSysUri().forEach(System.out::println);
    }
}
