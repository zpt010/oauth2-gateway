package com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.SysUri;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysUriMapper extends BaseMapper<SysUri> {

    @Select("select role_code from sys_role sr , sys_role_uri mir " +
            "where sr.role_id = mir.role_id and mir.sys_uri_id = #{uriId}")
    List<String> getUriRoles(int uriId);

    @Select("select * from sys_uri " )
    List<SysUri> getAllUri();

}
