package com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.SecurityUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<SecurityUser> {


    @Select("select role_code from sys_role ro, sys_user_role sr" +
            " where ro.role_id = sr.role_id and sr.user_id = #{userId}")
    List<String> getUserRoles(String userId);



}
