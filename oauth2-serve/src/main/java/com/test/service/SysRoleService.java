package com.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.entity.SysRole;

public interface SysRoleService extends IService<SysRole> {
}
