package com.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.entity.SysUri;

import java.util.List;

public interface SysUriService extends IService<SysUri> {
    List<SysUri> getAllSysUri();
}
