package com.test.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.entity.SysUri;
import com.test.mapper.SysUriMapper;
import com.test.service.SysUriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysUriServiceImpl extends ServiceImpl<SysUriMapper , SysUri> implements SysUriService {
    @Autowired
    private SysUriMapper uriMapper;

//    @Override
//    public List<SysUri> getAllSysUri() {
//        return uriMapper.getAllUri();
//    }

    @Override
    public List<SysUri> getAllSysUri() {
        return uriMapper.getAllUri()
                .stream().peek(sysUri -> sysUri.setRoles(uriMapper.getUriRoles(sysUri.getId()))).collect(Collectors.toList());
    }



}
