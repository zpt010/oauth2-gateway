package com.test.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.entity.SecurityUser;
import com.test.mapper.UserMapper;
import com.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper , SecurityUser> implements UserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public SecurityUser getUserByUserName(String userName) {
        LambdaQueryWrapper<SecurityUser> queryWrapper = new QueryWrapper<SecurityUser>().lambda();
        queryWrapper.eq(SecurityUser::getUsername , userName);
        List<SecurityUser> users = userMapper.selectList(queryWrapper);

        if (users == null || users.size() == 0){
            return null;
        }

        if(users.size() > 1){
            throw new RuntimeException("数据库定义错误！");
        }
        SecurityUser securityUser = users.get(0);
        List<String> userRoles = userMapper.getUserRoles(securityUser.getId());
        securityUser.setAuthorities(authorityList(userRoles));
        return securityUser;
    }

    private List<SimpleGrantedAuthority> authorityList(List<String> roles) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles){
            authorities.add(new SimpleGrantedAuthority(role.toUpperCase()));
        }
        return authorities;
    }
}
