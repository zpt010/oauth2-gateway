package com.test.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.List;

@TableName("sys_uri")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SysUri {

    @TableId
    private int id;

    @TableField("uri")
    private String Uri;

    @TableField("remark")
    private String remark;

    @TableField("role_code")
    private List<String> roles;

}
